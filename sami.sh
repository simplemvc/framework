while [[ $# == 1 ]]
do
key="$1"

case $key in
    -c|--clean)
        CLEAN=true
        shift
        ;;
esac
shift
done

if [ "$CLEAN" = true ] ; then
    if [[ -e 'api' ]] ; then
        rm -rf api
        rm -rf cache
        rm -rf sami.phar
    else
        echo "There's nothing to clean!"
    fi
else
   curl -O http://get.sensiolabs.org/sami.phar
   php sami.phar update sami.php
fi

