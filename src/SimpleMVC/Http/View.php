<?php namespace SimpleMVC\Http;

use Philo\Blade\Blade;

use SimpleMVC\Utils\Configuration;

class View
{
    /**
     * The view to be loaded
     *
     * @var \Illuminate\View\View
     */
    private $_view;

    /**
     * The data to pass on to the view
     *
     * @var array
     */
    private $_data = array();

    /**
     * The Blade Templating object
     *
     * @var \Philo\Blade\Blade
     */
    private $_blade;

    /**
     * The Blade parsing Factory class
     *
     * @var \Illuminate\View\Factory
     */
    private $_factory;

    /**
     * Configuration instance
     *
     * @var \SimpleMVC\Utils\Configuration
     */
    private $_config;

    /**
     * Class Constructor // Creates new Blade and View instances
     * Creates View instance from parsed Blade instance
     *
     * @return void
     */
    public function __construct()
    {
        $this->_config = new Configuration;
        $this->_blade = new Blade($this->_config->from('app')->get('views'),
            $this->_config->from('app')->get('cache'));
        $this->_view = $this->_blade->view();
    }

    /**
     * Renders the view
     *
     * @param string $path
     * @return void
     */
    public function render($path)
    {
       $this->parseContent($path, $this->_data);
       echo $this->_factory->render();
    }

    /**
     * Adds data to pass onto the view
     *
     * @param string $key
     * @param string $value
     * @return \SimpleMVC\Framework\Html\View
     */
    public function with($key, $value = null)
    {
        if (is_array($key)) {
            $this->_data = array_merge($this->_data, $key);
        } else {
            $this->_data[$key] = $value;
        }
        
        return $this;
    }

    /**
     * Checks whether or not data is being passed to
     * the view
     *
     * @return boolean
     */
    public function hasData()
    {
        if(empty($this->_data))
            return true;
        else
            return false;
    }

    /**
     * Parses the view using the Blade Factory
     *
     * @param string $path
     * @param string $data
     * @return void
     */
    private function parseContent($path, $data)
    {
        $path = str_replace('.', '/', $path);
        $this->_factory = $this->_view->make($path, $data);
    }
}
