<?php namespace SimpleMVC\Http;

class Errors extends Controller
{
    /**
     * Default 404 error
     *
     * @return void
     */
    public function error404()
    {
        die('The page you requested does not exist!');
    }
}