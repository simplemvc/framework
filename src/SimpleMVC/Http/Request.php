<?php namespace SimpleMVC\Http;

class Request
{
    /**
     * The request object
     *
     * @var \SimpleMVC\Http\Request
     */
    private $_request;

    /**
     * If the request is empty
     *
     * @var boolean
     */
    private $_empty;

    /**
     * Class Constructor // Casts request array to 
     * new Request object if given
     *
     * @param array $request
     * @return void
     */
    public function __construct($request = array())
    {
        $this->cast($request);
    }

    /**
     * Casts the request array into new Request object
     *
     * @param array $request
     */
    public function cast($request)
    {
        if(is_array($request) || is_object($request))
        {
            foreach($request as $key => $value)
            {
                if(is_array($value))
                    $this->cast($value);
                else
                    $this->$key = $value;
            }
        }
    }

    /**
     * Returns the request as a new Request object
     *
     * @return Request
     */
    public function getObject()
    {
        return new Request($this->_request);
    }

    /**
     * Sets the request
     *
     * @param array $request
     * @return Request
     *
     * @throws \Exception
     */
    public function setRequest($request = array())
    {
        if(!empty($request))
        {
            if(is_array($request))
            {
                $this->_request = $request;
                return $this;
            }
            else
            {
                throw new \Exception('Initial request object must be an instance of an array!');
            }
        }
        else
        {
            throw new \Exception('An empty request object was given!');
        }
    }
}