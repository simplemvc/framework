<?php namespace SimpleMVC\Http;

use SimpleMVC\Http\View;
use SimpleMVC\Http\Errors;

use SimpleMVC\Routing\Router;
use SimpleMVC\Routing\RouteLoader;

use SimpleMVC\Utils\UrlGenerator;

use Aliases;

class Controller
{
    /**
     * The view to be loaded
     *
     * @var \SimpleMVC\Http\View
     */
    private $_view;

    /**
     * The requested route for loading controllers
     *
     * @var array
     */
    private $_route;

    /**
     * URL Generator
     *
     * @var \SimpleMVC\Utils\UrlGenerator
     */
    private $_generator;

    /**
     * Class Contstructor // Creates new View instance
     *
     * @param array $route
     * @return void
     */
	public function __construct($route = array())
	{
		$this->_view = new View;
        $this->_generator = new UrlGenerator;

        if(!empty($route))
            $this->_route = $route;
	}

    /**
     * Renders a view
     *
     * @param string $path
     * @return \SimpleMVC\Http\Controller
     */
    protected function render($path)
    {
        $this->_view->render($path);

        return $this;
    }

    /**
     * Renders a view with data to pass on to the view
     *
     * @param string $path
     * @return \SimpleMVC\Http\Controller
     */
    protected function renderWithData($path)
    {
        $this->render($path);

        return $this;
    }

    /**
     * Adds data to the view class that will pass on to
     * the view
     *
     * @param string $key
     * @param string $value
     * @return \SimpleMVC\Http\Controller
     */
    protected function add($key, $value = null)
    {
        $this->_view->with($key, $value);
        return $this;
    }

    /**
     * Adds a flash message to be displayed once on rendering a view
     *
     * @param string $key
     * @param string $value
     * @return \SimpleMVC\Http\Controller
     *
     * @throws \Exception
     */
    protected function flash($key, $value)
    {
        if(empty($value))
            throw new \Exception('You must provide something as a value!');

        \Session::flash($key, $value);

        return $this;
    }

    /**
     * Redirect to a URL
     *
     * @param string $url
     * @return void
     */
    public function redirect($url)
    {
        $this->_generator->redirect($url);
    }

    /**
     * Redirects to a URL with data passed in as a temp flash session
     *
     * @param string $url
     * @param mixed $data
     * @return void
     */
    public function redirectWith($url, $data)
    {
        if(is_array($data))
        {
            foreach($data as $key => $value)
            {
                \Session::flashData($key, $value);
            }
        }
        else
            \Session::flashData('data', $data);

        $this->redirect($url);
    }

    /**
     * Loads the controller with the requested route
     *
     * @param \SimpleMVC\Routing\Router $router
     * @param string $basepath
     * @return void
     */
    public function load(Router $router, $basepath)
    {
        $loader = new RouteLoader;
        $loader->registerRouter($router);
        $this->_route = $loader->getRequestedRoute($basepath);

        if(isset($this->_route['method'])
            && $this->_route['method'] == $_SERVER['REQUEST_METHOD'])
        {
            $controller = 'App\Controllers\\' . $this->_route['controller'];
            $class = new $controller($this->_route);
            $method = $this->_route['action'];

            if(isset($this->_route['query']))
            {
                $request = new Request;
                $class->$method(
                    $request->setRequest(
                        $this->_route['query']
                    )->getObject()
                );
            }
            else
            {
                $class->$method();
            }
        }
        else
        {
            require_once \Config::get('app', 'errors');
            if(isset($errors))
            {
                foreach($errors as $error)
                {
                    $aexp = explode('@', $error);
                    $c = 'App\\Controllers\\'.$aexp[0];
                    $code = $aexp[1];
                    if(class_exists($c))
                    {
                        switch($code)
                        {
                            case '404':
                                $class = new $c;
                                $class->error404();
                                break;
                            default:
                                $class = new Errors;
                                $class->error404();
                                break;
                        }
                    }
                    else
                    {
                        $class = new Errors;
                        $class->error404();
                    }
                }
            }
            else
            {
                $class = new Errors;
                $class->error404();
            }
        }
    }

    /**
     * Gets the current loaded route
     *
     * @return array
     */
    protected function getRoute()
    {
        return $this->_route;
    }
}