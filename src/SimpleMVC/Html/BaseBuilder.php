<?php namespace SimpleMVC\Html;

class BaseBuilder
{
    /**
     * Cycles through given attributes and sets them for the element
     *
     * @param array $attributes
     * @return string
     */
    protected static function attributes($attributes=array())
    {
        $ar = array();

        foreach($attributes as $key => $value)
        {
            $element = self::attribute($key, $value);
            if(!is_null($element)) $ar[] = $element;
        }

        return count($ar) > 0 ? ' ' . implode(' ', $ar) : '';
    }

    /**
     * Creates the attribute string
     *
     * @param string $key
     * @param string $value
     * @return string
     */
    protected static function attribute($key, $value)
    {
        if(is_numeric($key)) $key = $value;

        if(!is_null($value)) return $key . '="' . $value . '"';
    }

    /**
     * Encodes a string using htmlentities
     *
     * @param string $value
     * @param boolean @safe
     * @return string
     */
    protected static function encode($value, $safe=false)
    {
        return ($safe) ? htmlentities($value, ENT_QUOTES, 'UTF-8') : $value;
    }

    /**
     * Decodes an HTML encoded string
     *
     * @param string $value
     * @return string
     */
    protected static function decode($value)
    {
        return html_entity_decode($value, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Obfuscates a string
     *
     * @param string $value
     * @return string
     */
    public static function obfuscate($value)
    {
        $safe = '';

        foreach(str_split($value) as $letter)
        {
            if(ord($letter) > 128) return $letter;

            switch(rand(1, 3))
            {
                case 1:
                    $safe .= '&#'.ord($letter).';';
                    break;
                case 2:
                    $safe .= '&#x'.dechex(ord($letter)).';';
                    break;
                case 3:
                    $safe .= $letter;
            }
        }

        return $safe;
    }
}
