<?php namespace SimpleMVC\Html;

use SimpleMVC\Utils\UrlGenerator;

class HtmlBuilder extends BaseBuilder
{
    /**
     * URL Generator
     *
     * @var \SimpleMVC\Utils\UrlGenerator
     */
    private static $_generator;

    /**
     * Bootstraps alias
     *
     * @return void
     */
    public static function boot()
    {
        self::$_generator = new UrlGenerator;
    }

    /**
     * Returns an asset. JS, CSS, image, or anything
     * from the assets directory
     *
     * @param string $path
     * @return string
     */
    public static function asset($path)
    {
        return self::$_generator->asset($path);
    }

    /**
     * Returns a URL
     *
     * @param string $path
     * @return string
     */
    public static function url($path)
    {
        // return self::$_basePath . $path;
        return self::$_generator->to($path);
    }

    /**
     * Generates an anchor tag
     *
     * @param string $path
     * @param string $content
     * @param array $options
     * @param boolean $safe
     * @return mixed
     */
    public static function link($url, $content='', $options=array(), $safe=false)
    {
        if(is_null($content)) $content = $url;

        return '<a href="'.self::$_generator->generateUrl($url).'"'.self::attributes($options).'>'.self::encode($content, $safe).'</a>';
    }

    /**
     * Obfuscates an email address
     *
     * @param string $email
     * @return string
     */
    public static function email($email)
    {
        return str_replace('@', '&#64;', self::obfuscate($email));
    }

    /**
     * Creates an image element
     *
     * @param string $url
     * @param string $alt
     * @param array $options
     * @return string
     */
    public static function image($url, $alt=null, $options=array())
    {
        $options['alt'] = $alt;

        return '<img src="'.self::$_generator->asset($url).'"'.self::attributes($options).'>';
    }
}