<?php namespace SimpleMVC\Html;

use SimpleMVC\Utils\UrlGenerator;

class FormBuilder extends BaseBuilder
{
    /**
     * URL Generator
     *
     * @var \SimpleMVC\Utils\UrlGenerator
     */
    private static $_generator;

    /**
     * Bootstraps alias
     *
     * @return void
     */
    public static function boot()
    {
        self::$_generator = new UrlGenerator;
    }
    /**
     * Open a new form object
     *
     * @param array $options
     * @return string
     */
    public static function open($options=array())
    {
        $options['method'] = self::getMethod($options);

        $options['action'] = self::getAction($options);

        $options['accept-charset'] = 'UTF-8';

        if(isset($options['files']) && $options['files'])
            $options['enctype'] = 'multipart/form-data';

        $options = self::attributes($options);

        return '<form'.$options.'>';
    }

    /**
     * Creates a form label
     *
     * @param string $for
     * @param string $content
     * @param array $options
     * @return string
     */
    public static function label($for, $content, $options=array())
    {
        return '<label for="'.$for.'"'.self::attributes($options).'>'.$content.'</label>';
    }

    /**
     * Creates a form input element
     *
     * @param string $type
     * @param string $name
     * @param string $value
     * @param array $options
     * @return string
     */
    public static function input($type, $name, $value='', $options=array())
    {
        if(!isset($options['name'])) $options['name'] = $name;
        
        $id = self::getId($options);

        if(!empty($value))
            $options['value'] = $value;

        $options['type'] = $type;

        return '<input'.self::attributes($options).'>';
    }

    /**
     * Creates a text form input box
     *
     * @param string $name
     * @param array $options
     * @return string
     */
    public static function text($name, $value='', $options=array())
    {
        return self::input('text', $name, $value, $options);
    }

    /**
     * Creates a hidden form input
     *
     * @param string $name
     * @param string $value
     * @param array $options
     * @return string
     */
    public static function hidden($name, $value='', $options=array())
    {
        return self::input('hidden', $name, $value, $options);
    }

    /**
     * Creates a form submit button
     *
     * @param string $content
     * @param array $options
     * @return string
     */
    public static function submit($value='', $options=array())
    {
        return self::input('submit', null, $value, $options);
    }

    /**
     * Creates a form reset button
     *
     * @param string $value
     * @param array $options
     * @return string
     */
    public static function reset($value='', $options=array())
    {
        return self::input('reset', null, $value, $options);
    }

    /**
     * Closes the form
     *
     * @return string
     */
    public static function close()
    {
        return "</form>";
    }

    /**
     * Gets the form method
     *
     * @param array $options
     * @return string
     */
    private static function getMethod($options)
    {
        if(isset($options['method']))
            $method = $options['method'];
        else
            $method = 'POST';

        return $method;
    }

    /**
     * Gets the form action
     *
     * @param array $options
     * @return string
     */
    private static function getAction($options)
    {
        if(isset($options['action']))
            $action = self::$_generator->to($options['action']);
        else
            $action = $_SERVER['REQUEST_URI'];

        return $action;
    }

    /**
     * Gets an input's id, or sets it if not supplied
     *
     * @param array $options
     * @return string
     */
    private static function getId($options)
    {
        $id = '';
        if(!isset($options['id']))
            $id = $options['name'];
        else
            $id = $options['id'];

        return $id;
    }
}