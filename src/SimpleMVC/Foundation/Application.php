<?php namespace SimpleMVC\Foundation;

use SimpleMVC\Routing\Router;
use SimpleMVC\Routing\RouteLoader;

use SimpleMVC\Utils\Configuration;
use SimpleMVC\Utils\Aliases;

use SimpleMVC\Http\Controller;

class Application
{
	/**
	 * Configuration instance
	 *
	 * @var \SimpleMVC\Utils\Config
	 */
	private $_config;

	/**
	 * The application router
	 *
	 * @var \SimpleMVC\Routing\Router
	 */
	private $_router;

	/**
	 * The base controller
	 *
	 * @var \SimpleMVC\Http\Controller
	 */
	private $_controller;

	/**
	 * The aliases class
	 *
	 * @var \SimpleMVC\Utils\Aliases
	 */
	private $_aliases;

	/**
	 * The applications main entry point. Responsible for
	 * executing the application
	 *
	 * @return void
	 */
	public function exec()
	{
		$this->loadCoreClasses();
		$this->registerAppDebug();
		$this->registerAliases();
		$this->registerRoutes();
		$this->registerControllers();
	}

	/**
	 * Registers the applications debugging state
	 *
	 * @return void
	 */
	public function registerAppDebug()
	{
		if($this->_config->get('debug'))
			error_reporting(E_ALL);
		else
			error_reporting(false);
	}

	/**
	 * Registers application routes
	 *
	 * @return void
	 */
	public function registerRoutes()
	{
		$this->_router->registerRoutes();
	}

	/**
	 * Gets a config file // Deprecated
	 *
	 * @param string $file
	 * @return mixed
	 * @deprecated
	 */
	public function getConfigFile($file)
	{
		return $this->_config->getConfigFile($file);
	}

	/**
	 * Registers the aliases
	 *
	 * @return void
	 */
	private function registerAliases()
	{
		$this->_aliases->registerAliases();
	}

	/**
	 * Responsible for assigning each base app variable to
	 * instances of their respective classes
	 *
	 * @return void
	 */
	private function loadCoreClasses()
	{
		$this->_config = new Configuration;
		$this->_aliases = new Aliases;
		$this->_router = new Router;
		$this->_controller = new Controller;

		// config get's special treatment
		// you need to load config here
		$this->_config->from('app');
	}

	/**
	 * Responsible for registering the app controllers
	 *
	 * @return void
	 */
	private function registerControllers()
	{
		$this->_controller->load($this->_router,
			$this->_config->get('basepath'));
	}
}