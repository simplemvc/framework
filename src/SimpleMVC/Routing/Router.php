<?php namespace SimpleMVC\Routing;

use SimpleMVC\Routing\Route;

use SimpleMVC\Utils\Configuration;

$router = ''; // Global router object for use in routes.php

class Router
{
    /**
     * The route object
     *
     * @var array
     */
    private $_route;

    /**
     * Configuration instance
     *
     * @var \SimpleMVC\Utils\Configuration
     */
    private $_config;

    private $_controllerPath; // unused...

    /**
     * Handles registering defined routes
     */
    public function registerRoutes()
    {
        $this->_route = new Route;
        $this->_config = new Configuration;

        $router = $this;
        require_once $this->_config->from('app')->get('routes');
    }

    /**
     * Handles registering the controllers path
     * Unused // Depericated
     *
     * @param string $path
     * @return void
     * @deprecated
     */
    public function registerControllersPath($path)
    {
        $exp = explode('/', $path);
        for($i = 0; $i < count($exp); $i++)
        {
            $exp[$i] = ucwords($exp[$i]);
        }
        $this->_controllerPath = implode('\\', $exp) . '\\';
        $this->_route->path($this->_controllerPath);
    }

    /**
     * Registers a GET request route
     *
     * @param string $url
     * @param mixed $action
     * @return void
     */
    public function get($url, $action)
    {
        $this->_route->register('GET', $url, $action);
    }

    /**
     * Registers a POST request route
     *
     * @param string $url
     * @param mixed $action
     * @return void
     */
    public function post($url, $action)
    {
        $this->_route->register('POST', $url, $action);
    }

    /**
     * Gets the route from supplied URL
     *
     * @param string $url
     * @return array
     */
    public function getRoute($url)
    {
        $routes = $this->_route->getRoutes();
        $return = '';
        foreach($routes as $route)
        {
            $routeURI = \Config::get('app', 'basepath').$route['uri'];

            // POST requests
            if(isset($_POST))
            {
                foreach($_POST as $key => $value)
                {
                    $route['query'][] = array($key => $value);
                }
            }

            // GET requests
            $getexp = explode('?', $_SERVER['REQUEST_URI']);
            if(isset($getexp[1]))
            {
                if(implode('?', $getexp) == $routeURI.'?'.$getexp[1])
                {
                    $route['querystring'] = $getexp[1];
                    $qexp = explode('&', $route['querystring']);
                    foreach($qexp as $q)
                    {
                        $i = explode('=', $q);
                        $route['query'][] = array($i[0] => $i[1]);
                    }
                }
            }

            if(isset($route['querystring']))
                $checkurl = $route['uri'] . '?' . $route['querystring'];
            else
                $checkurl = $route['uri'];

            if($checkurl == $url)
                return $route;
        }
    }

    /**
     * Gets a named route
     * Unfinished. Deprecated until then...
     *
     * @param string $name
     * @return void
     * @deprecated
     */
    public function getNamedRoute($name)
    {
        $routes = $this->_route->getRoutes();
        return $routes[$name];
    }
}