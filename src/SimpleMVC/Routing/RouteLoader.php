<?php namespace SimpleMVC\Routing;

use SimpleMVC\Routing\Route;
use SimpleMVC\Routing\Router;

class RouteLoader
{
    /**
     * The router
     *
     * @var \SimpleMVC\Routing\Router
     */
    private $_router;

    /**
     * Class Constructor // Sets class's router instance to
     * provided router
     *
     * @param \SimpleMVC\Routing\Router
     * @return void
     */
    public function registerRouter(Router $router)
    {
        $this->_router = $router;
    }

    /**
     * Gets the requested route for the controller
     *
     * @param string $basepath
     * @return array
     */
    public function getRequestedRoute($basepath)
    {
        $exp = explode($basepath, $_SERVER['REQUEST_URI']);
        return $this->_router->getRoute($exp[1]);
    }
}