<?php namespace SimpleMVC\Routing;

class Route
{
	/**
	 * The request methods allowed in each route registration
	 *
	 * @var array
	 */
	private $_methods = array('GET', 'POST', 'PUT', 'DELETE');

	/**
	 * Array every route is registered to
	 *
	 * @var array
	 */
	private $_routes = array();

	/**
	 * Registeres each route
	 *
	 * @param string $method
	 * @param string $url
	 * @param mixed $action
	 * @return void
	 *
	 * @throws \Exception
	 */
	public function register($method, $url, $action)
	{
		if(in_array($method, $this->_methods))
		{
			if(is_array($action))
			{
				if(!empty($action))
				{
					$exp = explode('@', $action['uses']);
					$this->_routes[] = array(
						'controller' => $exp[0],
						'action' => $exp[1],
						'method' => $method,
						'uri' => $url
					);
				}
				else
				{
					throw new \Exception('Cannot leave the action empty!');
				}
			}
			else
			{
				$exp = explode('@', $action);
				$this->_routes[] = array(
					'controller' => $exp[0],
					'action' => $exp[1],
					'method' => $method,
					'uri' => $url
				);
			}
		}
		else
		{
			throw new \Exception('Invalid routing method!');
		}
	}

	/**
	 * Gets all registered routes
	 *
	 * @return array
	 */
	public function getRoutes()
	{
		return $this->_routes;
	}

	/**
	 * Sets the controller path
	 *
	 * @param string $path
	 * @return void
	 * @deprecated
	 */
	public function path($controllerPath)
	{
		$this->_controllerPath = $controllerPath;
	}
}