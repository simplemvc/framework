<?php namespace SimpleMVC\Utils;

use SimpleMVC\Utils\Configuration;

class Aliases
{
	/**
	 * Array of aliases provided by application config
	 *
	 * @var array
	 */
	private $_aliases;

	/**
	 * Class Constructor // Sets aliases property to array
	 * from application config
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->_aliases = Config::get('app', 'aliases');
		$config = new Configuration;
		$this->_aliases = $config->from('app')->get('aliases');
	}

	/**
	 * Registers supplies aliases and executes it's
	 * boot sequence if one is given in each alias
	 *
	 * @return void
	 */
	public function registerAliases()
	{
		foreach($this->_aliases as $alias => $namespace)
		{
			class_alias($namespace, $alias);

			// Some aliases could have constructors
			// Load their boot methods
			$b = "\\$alias";
			if(method_exists($b, 'boot'))
				$b::boot();
		}
	}
}