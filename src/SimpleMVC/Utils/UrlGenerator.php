<?php namespace SimpleMVC\Utils;

class UrlGenerator
{
    /**
     * Generates a URL for local or remote assets
     *
     * @param string $url
     * @return string
     */
    public function asset($url)
    {
        if($this->isValidUrl($url))
            return $url;

        return $this->getRoot().'/assets/'.$url;
    }

    /**
     * Checks if a given URL is a valid one
     *
     * @param string $url
     * @return boolean
     */
    public function isValidUrl($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL);
    }

    /**
     * Returns the basepath of the application
     *
     * @return string
     */
    public function getRoot()
    {
        return \Config::get('app', 'basepath');
    }

    /**
     * Generates a URL useful for anchor tags
     *
     * @param string $url
     * @return string
     */
    public function to($url)
    {
        return ($this->isValidUrl($url)) ? $url : $this->getRoot().$url;
    }

    /**
     * Redirect to given URL
     *
     * @param string $url
     * @return void
     */
    public function redirect($url)
    {
        if(!$this->isValidUrl($url))
            $url = $this->getRoot() . $url;

        header('Location: ' . $url);
    }

    /**
     * Used to generate a URL based on relative or absolute
     *
     * @param string $url
     * @return string
     */
    public function generateUrl($url)
    {
        $extern = false;
        if((0 === strrpos($url, 'http://')) 
            || (0 === strrpos($url, 'https://')))
        {
            $extern = true;
        }

        if(!$extern)
            return $this->getRoot().$url;
        else
            return $url;
    }
}