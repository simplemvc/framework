<?php namespace SimpleMVC\Utils;

class ConfigFacade
{
	/**
	 * Gets a config option from the supplied file and
	 * given key
	 *
	 * @param string $file
	 * @param string $key
	 * @return mixed
	 */
	public static function get($file, $key)
	{
		$config = self::load($file);

		return $config[$key];
	}

	/**
	 * Loads the requested configuration file
	 *
	 * @param string $file
	 * @return mixed
	 */
	public static function load($file)
	{
		$location = getcwd().'/config/';
		$handle = opendir($location);
		while($files = readdir($handle))
		{
			if(!is_dir($files))
			{
				$filename = explode('.', $files)[0];
				if($filename == $file)
				{
					// echo $location.$files.'<br>';
					return require($location.'/'.$files);
				}
			}
		}
		closedir($handle);


		return false;
	}
}