<?php namespace SimpleMVC\Utils;

use SimpleMVC\Utils\ConfigFacade;

class Configuration
{
    /**
     * File to load
     *
     * @var string
     */
    protected $_configFile;

    /**
     * Config instance from request
     *
     * @var \SimpleMVC\Utils\Configuration
     */
    private static $_instance;

    /**
     * Gets a config instance statically from requested config
     *
     * @param string $from
     * @return \SimpleMVC\Utils\Configuration
     */
    public static function getInstance($from)
    {
        if(is_null(self::$_instance))
        {
            $c = new Configuration;
            self::$_instance = $c->from($from);
        }

        return self::$_instance;
    }

    /**
     * What file to load the config from
     *
     * @param string $file
     * @return \SimpleMVC\Utils\Configuration
     */
    public function from($file)
    {
        $this->_configFile = $file;

        return $this;
    }

    /**
     * Gets the config value
     *
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return ConfigFacade::get($this->_configFile, $key);
    }
}