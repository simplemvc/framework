<?php namespace SimpleMVC\Session;

interface SessionInterface
{
    /**
     * Checks the session has a specified key
     *
     * @param string $key
     * @return string
     */
	public static function get($key);

    /**
     * Adds to the session
     *
     * @param string $key
     * @param string $value
     * @return mixed
     */
	public static function set($key, $value);

    /**
     * Gets a value from the session
     *
     * @param string $key
     * @return boolean
     */
	public static function has($key);

    /**
     * Deletes a key from the session
     *
     * @param string $key
     * @return void
     */
	public static function delete($key);
}