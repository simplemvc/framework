<?php namespace SimpleMVC\Session;

class Session implements SessionInterface
{
	/**
	 * Checks the session has a specified key
	 *
	 * @param string $key
	 * @return boolean
	 */
	public static function has($key)
	{
		return (isset($_SESSION[$key])) ? true : false;
	}

	/**
	 * Adds to the session
	 *
	 * @param string $key
	 * @param string $value
	 * @return mixed
	 */
	public static function set($key, $value)
	{
		return $_SESSION[$key] = $value;
	}

	/**
	 * Gets a value from the session
	 *
	 * @param string $key
	 * @return mixed
	 */
	public static function get($key)
	{
		return $_SESSION[$key];
	}

	/**
	 * Deletes a key from the session
	 *
	 * @param string $key
	 * @return void
	 */
	public static function delete($key)
	{
		if(self::has($key))
			unset($_SESSION[$key]);
	}

	/**
	 * Creates a flash message by setting a session key
	 * And removing it upon being called again with the
	 * Specified key
	 *
	 * @param string $key
	 * @param string $value
	 * @return mixed
	 */
	public static function flash($key, $value='')
	{
		if(!empty($value))
		{
			self::set($key, $value);
		}
		else
		{
			if(self::has($key))
			{
				$s = self::get($key);
				self::delete($key);
				return $s;
			}
		}
	}

	/**
	 * Sets a session flash for redirection data
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return void
	 */
	public static function flashData($key, $value)
	{
		$$key = $key;
		self::flash($$key, $value);
	}
}