<?php namespace SimpleMVC\Database;

use SimpleMVC\Utils\Configuration;

use SimpleMVC\Database\Formatters\MySQL as MySQLFormatter;
use SimpleMVC\Database\Formatters\SQLite as SQLiteFormatter;

class Database extends \PDO
{
    /**
     * Configuration instance
     *
     * @var \SimpleMVC\Utils\Configuration
     */
    private $_config;

    /**
     * SQL Formatter
     *
     * @var mixed
     */
    private $_formatter;

    /**
     * Responsible for opening PDO connection
     *
     * @return void
     */
    public function __construct()
    {
        $c = new Configuration;
        $this->_config = $c->from('database');

        if(strtolower($this->_config->get('driver'))=='mysql')
        {
            parent::__construct(
                "mysql:host="
                .$this->_config->get('host').";dbname="
                .$this->_config->get('name').";charset=utf8mb4", 
                $this->_config->get('username'),
                $this->_config->get('password')
            );

            $this->_formatter = new MySQLFormatter;
        }
        elseif(strtolower($this->_config->get('driver'))=='sqlite')
        {
            $this->createSQLiteFile();
            parent::__construct('sqlite:' . $this->_config->get('sqlite_file'));

            $this->_formatter = new SQLiteFormatter;
        }
    }

    public function getTable($table)
    {
        if(strtolower(
            $this->_config->get('driver')) == 'mysql')
            return $this->_config->get('prefix').$table;
        else
            return $table;
    }

    /**
     * Creates a SQLite file if one is not found, and SQLite driver is in use
     *
     * @return void
     */
    private function createSQLiteFile()
    {
        if(!file_exists($this->_config->get('sqlite_file')))
        {
            $file = fopen($this->_config->get('sqlite_file'), 'w');
            fwrite($file, '');
            fclose($file);
        }
    }

    /**
     * Returns the formatter being used
     *
     * @return mixed
     */
    public function getFormatter()
    {
        return $this->_formatter;
    }

    /**
     * Executes table creation SQL script
     *
     * @param string $table
     * @param string $migration
     * @return void
     */
    public function create($table, $migration)
    {
        $this->execute($this->_formatter->create($table, $migration));
    }

    /**
     * Executes table alteration SQL script
     *
     * @param string $table
     * @param string $migration
     * @return void
     */
    public function update($table, $migration)
    {
        $this->execute($this->_formatter->update($table, $migration));
    }

    /**
     * Executes table truncation SQL script
     *
     * @param string $table
     * @return void
     */
    public function drop($table)
    {
        $this->execute($this->_formatter->drop($table));
    }

    /**
     * Executes given SQL script
     *
     * @param string $sql
     * @return void
     *
     * @throws \Exception
     */
    public function execute($sql)
    {
        try
        {
            $this->exec($sql);
            print_r($sql);
        }
        catch(\PDOException $ex)
        {
            throw new \Exception($ex->getMessage());
        }
    }
}