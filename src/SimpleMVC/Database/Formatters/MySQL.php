<?php namespace SimpleMVC\Database\Formatters;

use SimpleMVC\Utils\Configuration;

class MySQL
{
    /**
     * Configuration
     *
     * @var \SimpleMVC\Utils\Configuration
     */
    private $_config;

    private $_currentColumn;

    public $_columnToRemove;

    /**
     * Class Construct
     *
     * @return void
     */
    public function __construct()
    {
        $c = new Configuration;
        $this->_config = $c->from('database');
    }

    /**
     * Creates MySQL script for creating new tables
     *
     * @param string $table
     * @param string $migration
     * @return string
     */
    public function create($table, $migration)
    {
        return "CREATE TABLE IF NOT EXISTS `"
        .$table
        ."` ($migration) ENGINE=InnoDB DEFAULT"
        ." CHARSET=utf8 COLLATE utf8_unicode_ci;";
    }

    /**
     * Creates MySQL script for dropping tables
     *
     * @param string $table
     * @return string
     */
    public function drop($table)
    {
        return "DROP TABLE `".$table."`;";
    }

    /**
     * Creates MySQL script for updating tables
     *
     * @param string $table
     * @param string $migration
     * @return string
     */
    public function update($table, $migration)
    {
        return "ALTER TABLE `main`.`$table` ADD COLUMN $migration;";
    }

    /**
     * Creates MySQL script for adding primary index
     *
     * @param string $id
     * @return string
     */
    public function increments($id)
    {
        $this->_currentColumn = $id;
        return "`$id` int(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT";
    }

    public function integer($integer)
    {
        $this->_currentColumn = $integer;
        return "`$integer` int(10) UNSIGNED NOT NULL";
    }

    /**
     * Creates MySQL script for adding strings
     *
     * @param string $string
     * @return string
     */
    public function string($string)
    {
        $this->_currentColumn = $string;
        return "`$string` varchar(255) COLLATE utf8_unicode_ci NOT NULL";
    }

    /**
     * Creates MySQL script for adding text
     *
     * @param string $text
     * @return string
     */
    public function text($text)
    {
        $this->_currentColumn = $text;
        return "`$text` text COLLATE utf8_unicode_ci NOT NULL";
    }

    /**
     * Creates MySQL script for adding timestamps
     *
     * @param string $timestamp
     * @return string
     */
    public function timestamp($timestamp)
    {
        $this->_currentColumn = $timestamp;
        return "`$timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'";
    }

    /**
     * Unimplemented
     *
     * @param string $column
     * @throws \Exception
     */
    public function after($table, $column, $sql)
    {
        // throw new \Exception("after method not implemented yet!");
        // return "AFTER `$column`";
        return "ALTER TABLE `$table` ADD $sql AFTER `$column`;";
    }

    public function getLastColumnSql($migration)
    {
        for($i = 0; $i < count($migration); $i++)
        {
            echo $migration[$i]."\n";
            $exp = explode('`', $migration[$i]);
            print_r($exp);
        }
        // for($i = 0; $i < count($migration); $i++)
        // {
        //     $exp = explode('`', $migration[$i]);
        //     print_r($exp);
        //     if($exp[1] == $this->_currentColumn)
        //     {
        //         $this->_columnToRemove = $i;
        //         return implode('`', $exp);
        //     }
        // }
    }
}