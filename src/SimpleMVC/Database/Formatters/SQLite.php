<?php namespace SimpleMVC\Database\Formatters;

use SimpleMVC\Utils\Configuration;

use SimpleMVC\Database\Exceptions\SQLiteException;

class SQLite
{
    /**
     * Configuration
     *
     * @var \SimpleMVC\Utils\Configuration
     */
    private $_config;

    private $_currentColumn;

    /**
     * Class Construct
     *
     * @return void
     */
    public function __construct()
    {
        $c = new Configuration;
        $this->_config = $c->from('database');
    }

    /**
     * Creates SQLite script for creating new tables
     *
     * @param string $table
     * @param string $migration
     * @return string
     */
    public function create($table, $migration)
    {
        return "CREATE TABLE IF NOT EXISTS `main`.`"
        .$this->_config->get('prefix')."$table` ($migration);";
    }

    /**
     * Creates SQLite script for dropping tables
     *
     * @param string $table
     * @return string
     */
    public function drop($table)
    {
        return "DROP TABLE `".$this->_config->get('prefix').$table."`;";
    }

    /**
     * Creates SQLite script for updating tables
     *
     * @param string $table
     * @param string $migration
     * @return string
     */
    public function update($table, $migration)
    {
        return "ALTER TABLE `main`.`$table` ADD COLUMN $migration;";
    }

    /**
     * Creates SQLite script for adding primary index
     *
     * @param string $id
     * @return string
     */
    public function increments($id)
    {
        $this->_currentColumn = $id;
        return "`$id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL";
    }

    /**
     * Creates SQLite script for adding strings
     *
     * @param string $string
     * @return string
     */
    public function string($string)
    {
        $this->_currentColumn = $string;
        return "`$string` VARCHAR";
    }

    /**
     * Creates SQLite script for adding text
     *
     * @param string $text
     * @return string
     */
    public function text($text)
    {
        $this->_currentColumn = $text;
        return "`$text` TEXT";
    }

    /**
     * Creates SQLite script for adding timestamps
     *
     * @param string $timestamp
     * @return string
     */
    public function timestamp($timestamp)
    {
        $this->_currentColumn = $timestamp;
        return "`$timestamp` DATETIME";
    }

    /**
     * Unsupported. Here to keep up with migration requirements
     *
     * @param string $column
     * @return void
     */
    public function after($table, $column)
    {
        print_r("\"after\" option is not supported with SQLite. Cannot place \"{$this->_currentColumn}\" after requested column \"$column\"\n");
    }

    public function getLastColumnSql($migration)
    {
        foreach($migration as $column)
        {
            $exp = explode('`', $column);
            if($exp[1] == $this->_currentColumn)
            {
                return implode('`', $exp);
            }
        }
    }
}