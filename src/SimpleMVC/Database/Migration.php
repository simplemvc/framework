<?php namespace SimpleMVC\Database;

class Migration
{
    /**
     * Creates table from migration
     *
     * @param string $table
     * @param \Closure $closure
     * @return void
     */
    public function create($table, \Closure $closure)
    {
        $blueprint = $this->createBlueprint($table);
        $closure($blueprint);
        $blueprint->create();
    }

    /**
     * updates table from migration
     *
     * @param string $table
     * @param \Closure $closure
     * @return void
     */
    public function update($table, \Closure $closure)
    {
        $blueprint = $this->createBlueprint($table);
        $closure($blueprint);
        $blueprint->update();
    }

    /**
     * Drops table from migration
     *
     * @param string $table
     * @param \Closure $closure
     * @return void
     */
    public function drop($table)
    {
        $blueprint = $this->createBlueprint($table);
        $blueprint->drop();
    }

    /**
     * Creates a new blueprint instance
     *
     * @param string $table
     * @return \SimpleMVC\Database\Blueprint
     */
    protected function createBlueprint($table)
    {
        return new Blueprint($table);
    }
}