<?php namespace SimpleMVC\Database;

use SimpleMVC\Database\Database;

class Blueprint
{
    /**
     * The table to manipulate
     *
     * @var string
     */
    private $_table;

    /**
     * The migration array
     *
     * @var string
     */
    private $_migration = array();

    /**
     * The database instance
     *
     * @var \SimpleMVC\Database\Database
     */
    private $_database;

    /**
     * The SQL formatter instance
     *
     * @var mixed
     */
    private $_formatter;

    /**
     * Class Constructor
     *
     * @return void
     */
    public function __construct($table)
    {
        $this->_database = new Database;
        $this->_formatter = $this->_database->getFormatter();
        $this->_table = $this->_database->getTable($table);
    }

    /**
     * Sends complete SQL script to Database for creation
     *
     * @return void
     */
    public function create()
    {
        $migration = "";
        foreach($this->_migration[0] as $column)
        {
            if(!empty($column))
                $migration .= "$column, ";
        }
        $migration = rtrim($migration, ', ');
        $migration = str_replace(', AFTER', ' AFTER', $migration);
        $this->_database->create($this->_table, $migration);

        foreach($this->_migration[1] as $sql)
        {
            $this->_database->exec($sql);
        }
    }

    /**
     * Sends complete SQL script to Database for alteration
     *
     * @return void
     */
    public function update()
    {
        foreach($this->_migration as $migration)
        {
            $this->_database->update($this->_table, $migration);
        }
    }

    /**
     * Sends complete SQL script to Database for truncation
     *
     * @return void
     */
    public function drop()
    {
        $this->_database->drop($this->_table);
    }

    /**
     * Adds primary key SQL script to migration array
     *
     * @param string $id
     * @return \SimpleMVC\Databse\Blueprint
     */
    public function increments($id)
    {
        $this->_migration[0][] = $this->_formatter->increments($id);
        return $this;
    }

    /**
     * Adds integer SQL script to migration array
     *
     * @param string $integer
     * @return \SimpleMVC\Databse\Blueprint
     */
    public function integer($integer)
    {
        $this->_migration[0][] = $this->_formatter->integer($integer);
        return $this;
    }

    /**
     * Adds string SQL script to migration array
     *
     * @param string $string
     * @return \SimpleMVC\Databse\Blueprint
     */
    public function string($string)
    {
        $this->_migration[0][] = $this->_formatter->string($string);
        return $this;
    }

    /**
     * Adds text SQL script to migration array
     *
     * @param string $text
     * @return \SimpleMVC\Databse\Blueprint
     */
    public function text($text)
    {
        $this->_migration[0][] = $this->_formatter->text($text);
        return $this;
    }

    /**
     * Adds timestamp SQL script to migration array
     *
     * @param string $timestamp
     * @return \SimpleMVC\Databse\Blueprint
     */
    public function timestamps()
    {
        $this->_migration[0][] = $this->_formatter->timestamp('created_at');
        $this->_migration[0][] = $this->_formatter->timestamp('updated_at');
        return $this;
    }

    /**
     * Makes sure a column is placed after given column
     *
     * @param string $column
     * @return \SimpleMVC\Databse\Blueprint
     */
    public function after($column)
    {
        // $this->_migration[1][] = $this->_formatter->after($this->_table, $column);

        $c = $this->_formatter->getLastColumnSql($this->_migration[0]);
        // print_r($c."\n");
        unset($this->_migration[0][
            $this->_database->getFormatter()->_columnToRemove
        ]);

        $this->_migration[1][] = $this->_formatter->after($this->_table, $column, $c);

        print_r($this->_migration);

        return $this;
    }
}