<?php namespace SimpleMVC\Database\Exceptions;

class SQLiteException extends \Exception
{
    /**
     * Class Constructor // Registers exception
     *
     * @param string $message
     * @param integer $code
     * @return void
     */
    public function __construct($message, $code=0)
    {
        parent::__construct($message, $code);
    }

    /**
     * Returns the message as a string
     *
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": [$this->code}]: {$this->message}\n";
    }
}