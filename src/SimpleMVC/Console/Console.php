<?php namespace SimpleMVC\Console;

class Console
{
    /**
     * Register console command class
     *
     * @param string $command
     * @return void
     */
    public function register($command)
    {
        $classexp = explode('/', $command);
        for($i = 0; $i < count($classexp); $i++)
            $classexp[$i] = ucwords($classexp[$i]);

        $class = __NAMESPACE__.'\\'.implode('\\', $classexp);
        return new $class();
    }
}