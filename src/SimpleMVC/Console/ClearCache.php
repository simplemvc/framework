<?php namespace SimpleMVC\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ClearCache extends Command
{
    /**
     * Configure Symfony Command
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('clear-cache')
             ->setDescription('Clears the cache');
    }

    /**
     * Execute console command
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input,
        OutputInterface $output)
    {
        $cache = getcwd() . '/storage/cache/';

        $handle = opendir($cache);
        $ignore = array('.','..','.gitkeep');
        while($file = readdir($handle))
        {
            if(!in_array($file, $ignore))
            {
                $output->writeln('Clearing file ' . $file . ' from the cache');
                unlink($cache.$file);
            }
        }
    }
}