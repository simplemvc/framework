<?php namespace SimpleMVC\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class CreateController extends Command
{
    /**
     * Configure Symfony Command
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('controller')
             ->setDescription('Manage Controllers')
             ->setDefinition(
                new InputDefinition(array(
                    new InputArgument('option', InputArgument::REQUIRED),
                    new InputArgument('name', InputArgument::REQUIRED)
                ))
            );
    }

    /**
     * Execute console command
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input,
        OutputInterface $output)
    {
        $option = $input->getArgument('option');
        $name = $input->getArgument('name');

        $content = <<<EOF
<?php namespace App\Controllers;

use SimpleMVC\Http\Controller;
use SimpleMVC\Http\Request;

class $name extends Controller
{
    // Place methods here
}
EOF;
        
        $cdir = getcwd().'/app/controllers/';
        $filename = $name.'.php';
        $filepath = $cdir.$filename;

        if(strtolower($option)=='new')
        {
            $file = fopen($filepath, 'w');
            fwrite($file, $content);
            fclose($file);

            $output->writeln('Controller: '.$name.' created.');
        }
        elseif(strtolower($option)=='delete')
        {
            unlink($filepath);
            $output->writeln($filename.' was deleted!');
        }
    }
}