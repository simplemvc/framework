<?php namespace SimpleMVC\Console\Database;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MigrationBuilder extends Command
{
    /**
     * Configure Symfony Command
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('db:make')
             ->setDescription('Creates a migration')
             ->addArgument('table', InputArgument::REQUIRED, 'The table to create the migration for');
    }

    /**
     * Execute console command
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input,
        OutputInterface $output)
    {
        $table = $input->getArgument('table');

        $timestamp = date('Y_m_d_his').'_';

        $fileName = getcwd().'/migrations/'.$timestamp.$table.'.php';

        $exp = explode('_', $table);
        $action = $exp[0];
        $tableName = $exp[1];
        for($i = 0; $i < count($exp); $i++)
        {
            $exp[$i] = ucwords($exp[$i]);
        }
        $table = implode('', $exp);

        $content = <<<EOF
<?php

use SimpleMVC\Database\Blueprint;
use SimpleMVC\Database\Migration;

class $table extends Migration
{
    /**
     * For pushing migrations up
     *
     * @return void
     */
    public function up()
    {
        \$this->$action('$tableName', function(Blueprint \$table)
        {
            // Place table rows here
        });
    }

    /**
     * For reversing migrations
     *
     * @return void
     */
    public function down()
    {
        // This is for removing the table
    }
}

EOF;

        $file = fopen($fileName, 'w');
        fwrite($file, $content);
        fclose($file);

        $this->writeToMigrationsFile($fileName);

        $output->writeln('Created migration: ' . $fileName . ' successfully.');
    }

    private function writeToMigrationsFile($file)
    {
        $cacheFile = getcwd().'/storage/migrations';

        $cfg = fopen($cacheFile, 'a');
        fwrite($cfg, $file . "\n");
        fclose($cfg);
    }
}