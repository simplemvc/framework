<?php namespace SimpleMVC\Console\Database;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

use SimpleMVC\Utils\Configuration as Config;

class Reset extends Command
{
    /**
     * Config instance
     *
     * @var \SimpleMVC\Utils\Configuration
     */
    private $_config;

    /**
     * Configure Symfony Command
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('db:reset')
             ->setDescription('Resets all changes to database');

        $this->_config = Config::getInstance('database');
    }

    /**
     * Execute console command
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input,
        OutputInterface $output)
    {
        if(strtolower(
            $this->_config->get('driver')) == 'mysql')
        {
            $output->writeln("<error>This command is not supported on MySQL databases, only SQLite!</error>");
        }
        else
        {
            unlink($this->_config->get('sqlite_file'));
            $file = fopen($this->_config->get('sqlite_file'), 'w');
            fwrite($file, '');
            fclose($file);

            $output->writeln('SQLite database has been reset!');
        }
    }
}