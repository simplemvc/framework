<?php namespace SimpleMVC\Console\Database;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class Rollback extends Command
{
    /**
     * Configure Symfony Command
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('db:rollback')
             ->setDescription('Rollback migrations');
    }

    /**
     * Execute console command
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input,
        OutputInterface $output)
    {
        $migrationsTemplate = getcwd().'/storage/migrations';
        
        $file = file($migrationsTemplate);
        foreach($file as $item)
        {
            $nameexp = explode('_', $item);
            for($i = 0; $i < 4; $i++)
            {
                array_shift($nameexp);
            }

            for($i = 0; $i < count($nameexp); $i++)
            {
                $nameexp[$i] = ucwords($nameexp[$i]);
            }

            $name = str_replace('_', '', 
                str_replace('.php', '', implode('_', $nameexp))
            );

            require_once trim($item);
            
            $class = '\\'.trim($name);
            $c = new $class();
            $c->down();
        }
    }
}