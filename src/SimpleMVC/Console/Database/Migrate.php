<?php namespace SimpleMVC\Console\Database;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class Migrate extends Command
{
    /**
     * Configure Symfony Command
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('db:push')
             ->setDescription('Migrate database migrations');
    }

    /**
     * Execute console command
     *
     * @param Symfony\Component\Console\Input\InputInterface $input
     * @param Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input,
        OutputInterface $output)
    {
        $migrationsTemplate = getcwd().'/storage/migrations';
        
        $file = file($migrationsTemplate);
        foreach($file as $item)
        {
            $nameexp = explode('_', $item);
            for($i = 0; $i < 4; $i++)
            {
                array_shift($nameexp);
            }

            for($i = 0; $i < count($nameexp); $i++)
            {
                $nameexp[$i] = ucwords($nameexp[$i]);
            }

            $name = str_replace('_', '', 
                str_replace('.php', '', implode('_', $nameexp))
            );

            // require_once trim($item);

            if(file_exists(trim($item)))
            {
                $output->writeln("Migrating ".trim($name)." migration");
                require_once trim($item);
                $class = '\\'.trim($name);
                $c = new $class();
                $c->up();
            }
            else
            {
                $output->writeln("Item: ".trim($name)." could not be found. Skipping migration");
            }
        }
    }
}