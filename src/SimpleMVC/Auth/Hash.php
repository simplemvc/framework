<?php namespace SimpleMVC\Auth;

class Hash
{
	/**
	 * Spin up boot sequence of alias
	 *
	 * @return void
	 */
	public static function boot()
	{
		if(\Config::get('app', 'crypt_compat'))
			require_once $_SERVER['DOCUMENT_ROOT'] . \Config::get('app', 'basepath') . '/vendor/ircmaxell/password-compat/lib/password.php';
	}

	/**
	 * Hash a string
	 *
	 * @param string $string
	 * @return string
	 */
	public static function make($string)
	{
		return password_hash($string, PASSWORD_BCRYPT);
	}

	/**
	 * Check if a hash matches a given string
	 *
	 * @param string $string
	 * @param string $hash
	 * @return boolean
	 */
	public static function check($string, $hash)
	{
		return password_verify($string, $hash);
	}
}