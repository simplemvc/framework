# SimpleMVC Framework
SimpleMVC is a PHP MVC Framework built to be lightweight and simple to use.

## What is this?
It's an MVC (Model, View, Controller) framework built in PHP from scratch. Why build one if there are already others out there? 2 reasons..

1. Dive head-first into building a large-scale application framework  
2. To serve the needs of what I need it for, and change it in any capacity as I see fit.  

## Can I use it now?
Yes/No. It's far from complete. There's still SOOOO many things to implement, and so much is missing, like...the M...No model support yet.

## What's currently implemented?
Basic operating components are implemented. It's not at all a complete MVC framework, as there's still much to do, but here's what it has so far:

1. Controllers - Pages are loaded via controllers
2. Views - You can create pages and layouts using [Laravel's](https://laravel.com/) [Blade Templating Engine](https://laravel.com/docs/5.2/blade) which is built into the framework via [PhiloNL's Standalone Blade Package](https://github.com/PhiloNL/Laravel-Blade)
3. Routing - The router currently only supports GET/POST requests, and doesn't support POST requests very well yet.
4. Rudementary Session Management - Basic session management with NO cookie support yet.

## Required PHP Depencencies
* [philo/laravel-blade](https://github.com/PhiloNL/Laravel-Blade) - The Blade Templating Engine
* [ircmaxell/password-compat](https://github.com/ircmaxell/password_compat) - password_* functions for older PHP version < 5.5


## Contributors
[Kaleb Klein](http://kalebklein.com)

## License
This framework is covered by the [GNU General Public License v2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)
