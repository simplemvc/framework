<?php

use Symfony\Component\Finder\Finder;
use Sami\Parser\Filter\TrueFilter;

$iterator = Finder::create()
	->files()
	->name('*.php')
	->in('src');

$sami = new Sami\Sami($iterator, array(
	'theme' => 'default',
	'title' => 'SimpleMVC API',
	'default_opened_level' => 1,
	'build_dir' => 'api/SimpleMVC/',
	'cache_dir' => 'cache/',
));

$sami['filter'] = function()
{
	return new TrueFilter();
};

return $sami;